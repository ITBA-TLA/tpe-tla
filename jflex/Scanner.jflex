package Example;
import java_cup.runtime.SymbolFactory;
import java.util.HashMap;
import java.util.Map;
%%
%cup

%class Scanner
%{
	private SymbolFactory sf;

	public Scanner(java.io.InputStream r, SymbolFactory sf){
		this(r);
		this.sf=sf;
	}
	
%}
%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}

%%
";"			{ return sf.newSymbol("Semicolon", sym.SEMI); }
"+"			{ return sf.newSymbol("Plus", sym.PLUS); }
"*"			{ return sf.newSymbol("Times", sym.TIMES); }
"("			{ return sf.newSymbol("Left Parent", sym.LPAREN); }
")"			{ return sf.newSymbol("Right Paren", sym.RPAREN); }
"-"			{ return sf.newSymbol("Minus", sym.MINUS); }
"/"			{ return sf.newSymbol("Divide", sym.DIVIDE); }
"%"			{ return sf.newSymbol("Module", sym.MODULE); }
"if"		{ return sf.newSymbol("If", sym.IF); }
"else"		{ return sf.newSymbol("Else", sym.ELSE); }
"while"		{ return sf.newSymbol("While", sym.WHILE); }
"do"		{ return sf.newSymbol("Do", sym.DO); }
"break"		{ return sf.newSymbol("Break", sym.BREAK); }
"return"	{ return sf.newSymbol("Return", sym.RETURN); }
"main"		{ return sf.newSymbol("Main", sym.MAIN); }
"{"			{ return sf.newSymbol("Left Bracket", sym.LBRACKET); }
"}"			{ return sf.newSymbol("Right Bracket", sym.RBRACKET); }
\"			{ return sf.newSymbol("Quote", sym.QUOTE); }
"'"			{ return sf.newSymbol("Single Quote", sym.SQUOTE); }
"&&"		{ return sf.newSymbol("And", sym.AND); }
"||"		{ return sf.newSymbol("Or", sym.OR); }
"<"			{ return sf.newSymbol("Less", sym.LESS); }
">"			{ return sf.newSymbol("Higher", sym.GREAT); }
"<="		{ return sf.newSymbol("Less", sym.LESSEQ); }
">="		{ return sf.newSymbol("Higher", sym.GREATEQ); }
"="			{ return sf.newSymbol("Asigns", sym.ASSIGN); }
"=="		{ return sf.newSymbol("Equal", sym.EQ); }
"write"		{ return sf.newSymbol("Equal", sym.WRITE); }
"read"		{ return sf.newSymbol("Equal", sym.READ); }
"!="		{ return sf.newSymbol("Not Equal", sym.NEQUAL); }
"int"		{ return sf.newSymbol("Integer", sym.INT); }
"string"	{ return sf.newSymbol("String", sym.STRING); }
\"(.*)\"		{ return sf.newSymbol("String", sym.STRING_CONSTANT, new String(yytext())); }
"?"			{ return sf.newSymbol("Question", sym.QUESTION); }
[A-Za-z_][A-Za-z0-9_]*	{ return sf.newSymbol("Id", sym.ID, new String(yytext())); }
0|[1-9][0-9]* 			{ return sf.newSymbol("Integral Number", sym.NUMBER, new Integer(yytext())); }
[ \t\r\n\f]				{ /* ignore white space. */ }
. 						{ System.err.println("Illegal character: "+yytext()); }
