main{
	int t;
	int i;
	int xPrev;
	int xCurr;
	int aux;

	write "[Fibonacci - Terminators]";
	write "Ingrese un numero:\n";
	read i;

	xPrev = 0;
	xCurr = 1;

	if (i < 0) {
		write "Error";
		return; 
	} else {}

	if (i == 0) {
		write xPrev;
		return;
	} else {}

	if (i == 1) {
		write xCurr;
		return;
	} else {}

	while (i > 1) {
		aux = xCurr;
		xCurr = xCurr + xPrev;
		xPrev = aux;

		i = i - 1;
	}

	write xCurr;
}
